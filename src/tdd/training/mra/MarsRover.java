package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private int roverX;
	private int roverY;
	private String facing;
	
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.roverX = 0;
		this.roverY = 0;
		this.facing = "N";
		
	}
	
	
	public int getRoverX() {
		return roverX;
	}
	
	
	public void setRoverX(int roverX) {
		this.roverX = roverX;
	}
	
	
	public int getRoverY() {
		return roverY;
	}
	
	
	public void setRoverY(int roverY) {
		this.roverY = roverY;
	}
	
	
	public String getFacing() {
		return facing;
	}
	
	
	public void setFacing(String facing) {
		this.facing = facing;
	}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		for(String obstacle : planetObstacles) {
			if((obstacle.charAt(1) - '0') == x && (obstacle.charAt(3) - '0') == y) {
				return true;
			}
			
		}
		
		return false ;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		String commands[] = commandString.split("(?!^)");
		
		for(String command : commands) {
		
			if(command.equals("")) {
				return "(" + roverX + "," + roverY + "," + facing + ")";
			}else if(command.equals("r")){
				if(facing.equals("N")) {
					facing = "E";
				}else if(facing.equals("E")) {
					facing = "S";
				}else if(facing.equals("S")) {
					facing = "W";
				}else if(facing.equals("W")) {
					facing = "N";
				}
			}else if(command.equals("l")) {
				if(facing.equals("N")) {
					facing = "W";
				}else if(facing.equals("W")) {
					facing = "S";
				}else if(facing.equals("S")) {
					facing = "E";
				}else if(facing.equals("E")) {
					facing = "N";
				}
			}else if(command.equals("f")) {
				if(facing.equals("N")) {
					if(roverY == planetY-1) {
						roverY = 0;
					}else {	roverY = roverY + 1; }					
				}else if(facing.equals("W")) {
					if(roverX == 0) {
						roverX = planetX - 1;
					}else { roverX = roverX - 1; }
				}else if(facing.equals("S")) {
					if(roverY == 0) {
						roverY = planetY - 1;
					}else { roverY = roverY - 1; }
				}else if(facing.equals("E")) {
					if(roverX == planetX - 1) {
						roverX = 0;
					}else { roverX = roverX + 1; }
				}
			}else if(command.equals("b")) {
				if(facing.equals("N")) {
					if(roverY == 0) {
						roverY = planetY - 1;
					}else { roverY = roverY - 1; }
				}else if(facing.equals("W")) {
					if(roverX == planetX - 1) {
						roverX = 0;
					}else { roverX = roverX + 1; }
				}else if(facing.equals("S")) {
					if(roverY == planetY - 1) {
						roverY = 0;
					}else { roverY = roverY + 1; }
				}else if(facing.equals("E")) {
					if(roverX == 0) {
						roverX = planetX - 1;
					}else { roverX = roverX - 1; }
				}
			}
		}
		
		return "(" + roverX + "," + roverY + "," + facing + ")";
	}

}
