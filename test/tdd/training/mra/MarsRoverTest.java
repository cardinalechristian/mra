package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void cellContainsObstacleTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(4,7));
	}
	
	@Test
	public void cellNotContainsObstacleTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		assertFalse(rover.planetContainsObstacleAt(0,3));
	}
	
	@Test
	public void landingStatusWithEmptyCommandTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String commandString = "";
		assertEquals("(0,0,N)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void turnRoverToTheRightTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String commandString = "r";
		assertEquals("(0,0,E)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void turnRoverToTheLeftTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		String commandString = "l";
		assertEquals("(0,0,W)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverForwardToNordTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(7);
		rover.setRoverY(6);
		rover.setFacing("N");
		String commandString = "f";
		assertEquals("(7,7,N)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverForwardToWestTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(7);
		rover.setRoverY(6);
		rover.setFacing("W");
		String commandString = "f";
		assertEquals("(6,6,W)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverForwardToSudTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(7);
		rover.setRoverY(6);
		rover.setFacing("S");
		String commandString = "f";
		assertEquals("(7,5,S)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverForwardToEstTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(7);
		rover.setRoverY(6);
		rover.setFacing("E");
		String commandString = "f";
		assertEquals("(8,6,E)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverBackwardToNordTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(5);
		rover.setRoverY(8);
		rover.setFacing("N");
		String commandString = "b";
		assertEquals("(5,7,N)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverBackwardToWestTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(5);
		rover.setRoverY(8);
		rover.setFacing("W");
		String commandString = "b";
		assertEquals("(6,8,W)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverBackwardToSudTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(5);
		rover.setRoverY(8);
		rover.setFacing("S");
		String commandString = "b";
		assertEquals("(5,9,S)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverBackwardToEstTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(5);
		rover.setRoverY(8);
		rover.setFacing("E");
		String commandString = "b";
		assertEquals("(4,8,E)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void moveRoverWithCombinationComandsTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "ffrff";
		assertEquals("(2,2,E)",rover.executeCommand(commandString));
	}
	
	
	@Test
	public void wrappingRoverTest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "b";
		assertEquals("(0,9,N)",rover.executeCommand(commandString));
	}
	
}
